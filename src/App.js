import React, { Component } from "react";
import Menu from "./component/Menu/Menu";
import Sections from "./component/Sections/Sections";
import "./App.scss";
class App extends Component {
  state = {
    activeSectionId: "1"
  };
  setActiveSectionId = id => {
    this.setState({
      activeSectionId: id
    });
  };
  render() {
    const { setActiveSectionId } = this;
    const { activeSectionId } = this.state;
    return (
      <div className="App main">
        <div className="wrapper">
          <Menu activeSectionId={activeSectionId} />
          <Sections setActiveSectionId={setActiveSectionId} />
        </div>
      </div>
    );
  }
}

export default App;
