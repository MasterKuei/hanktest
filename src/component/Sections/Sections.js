import React, { Component } from "react";
import "./Sections.scss";
import TabsBox from "../Tabsbox/TabsBox";
import { Waypoint } from "react-waypoint";

const sectionData = [
  {
    id: "1",
    dec01: true,
    dec02: true,
    info: null,
    link: null
  },
  {
    id: "2",
    dec01: true,
    dec02: true,
    info: "挖掘自深海的神秘力量<br/>擁有操控電磁的強大威力<br/>CSO唯一史詩稀有度武器<br/>霸雷槍．天誅》正式登場<br/>",
    link: "https://event.beanfun.com/cso/EventAD/EventAD.aspx?EventADID=5232"
  },
  {
    id: "3",
    dec01: true,
    dec02: false,
    info:
      "在一港口地帶 追擊蜜拉．羅莉的露西亞艦隊<br/>因發現了可疑的軍事設施而開始進行搜查...<br/>蜜拉．羅莉和歐巴．史密斯會如何進行反擊？<br/>潛藏於海底的神祕機械巨齒鯊又是誰的爪牙？<br/>一同參戰全新災厄之章最終章《7-2英雄集結》！<br/>尚未完全掌握狀況，設定為「危險」等級進行特別管理<br />",
    link: "https://event.beanfun.com/cso/EventAD/EventAD.aspx?EventADID=5231"
  },
  {
    id: "4",
    dec01: true,
    dec02: true,
    info: "高人氣殭屍Z模式參戰確定<br/>即日起在自己最愛的地圖中<br/>盡情升等殺殭屍<br/>發揮創意 創造回憶<br />",
    link: "https://event.beanfun.com/cso/EventAD/EventAD.aspx?EventADID=5230"
  },
  {
    id: "5",
    dec01: true,
    dec02: false,
    info: null,
    link: "https://event.beanfun.com/cso/EventAD/EventAD.aspx?EventADID=5284",
    tabsBox: true
  },
  {
    id: "6",
    dec01: true,
    dec02: false,
    info: null,
    link: null
  }
];

const Section = ({ setActiveSectionId, ...sectionData }) => {
  return (
    <Waypoint
      topOffset="50%"
      bottomOffset="50.001%"
      scrollableAncestor={window}
      onEnter={() => {
        setActiveSectionId(sectionData.id);
      }}
    >
      <div id={`SE0${sectionData.id}`} className={`section section-0${sectionData.id}`}>
        <div className="section__content">
          {!sectionData.dec01 ? null : <div className="dec01" />}
          {!sectionData.dec02 ? null : <div className="dec02" />}
          {!sectionData.info ? null : (
            <div className="info">
              <div className="info__content" dangerouslySetInnerHTML={{ __html: sectionData.info }} />
            </div>
          )}
          {!sectionData.tabsBox ? null : <TabsBox />}
          {!sectionData.link ? null : (
            <a className="more_link" href={sectionData.link} target="_blank" rel="noopener noreferrer" />
          )}
        </div>
      </div>
    </Waypoint>
  );
};

class Sections extends Component {
  render() {
    const { setActiveSectionId } = this.props;
    return (
      <>
        {sectionData.map(section => {
          return <Section key={section.id} {...section} setActiveSectionId={setActiveSectionId} />;
        })}
      </>
    );
  }
}

export default Sections;
