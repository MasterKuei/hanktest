import React, { Component } from "react";
import { TweenMax } from "gsap/TweenMax";
import "gsap/src/uncompressed/plugins/ScrollToPlugin";
import "./Menu.scss";

const menuData = [
  {
    id: "2",
    href: "SE02",
    text: "史詩武器現身"
  },
  {
    id: "3",
    href: "SE03",
    text: "災厄全新7-2"
  },
  {
    id: "4",
    href: "SE04",
    text: "beanfun!的App祝福"
  },
  {
    id: "5",
    href: "SE05",
    text: "Studio殭屍Z登場"
  },
  {
    id: "6",
    href: "SE06",
    text: "豬年新年活動"
  }
];

class Item extends Component {
  aScrollTo = props => {
    const anchor = document.getElementById(props);
    const posTop = anchor.offsetTop;
    TweenMax.to(window, 0.5, {
      scrollTo: posTop
    });
  };
  handleClick = e => {
    e.preventDefault();
    this.aScrollTo(this.props.href);
  };
  render() {
    const { href, text, active } = this.props;
    return (
      <a href={href} className={active ? "active" : ""} onClick={this.handleClick}>
        {text}
      </a>
    );
  }
}

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      wH: window.pageYOffset,
      fixed: false,
      currentItem: 0
    };
  }
  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }
  handleScroll = () => {
    this.setState({
      wH: window.pageYOffset
    });
    if (this.state.wH >= 40) {
      this.setState({
        fixed: true
      });
    } else {
      this.setState({
        fixed: false
      });
    }
  };

  render() {
    const { activeSectionId } = this.props;
    return (
      <div className={`menu ${this.state.fixed ? "fixed" : ""}`}>
        <div className="menu__list">
          {menuData.map(menu => {
            return <Item active={menu.id === activeSectionId} key={menu.id} {...menu} />;
          })}
        </div>
      </div>
    );
  }
}

export default Menu;
