import React, { Component } from "react";
import "./TabsBox.scss";


const tabsData = [
  { id: "1", tabName: "祝福一", tabContent: "<img src='./content_tab1.png'/>" },
  { id: "2", tabName: "祝福二", tabContent: "<img src='./content_tab2.png'/>" },
  { id: "3", tabName: "祝福三", tabContent: "<img src='./content_tab3.png'/>" },
  { id: "4", tabName: "祝福四", tabContent: "<img src='./content_tab4.png'/>" }
];

class TabsBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabIndex: 0
    };
  }

  setTabShow = idx => {
    this.setState({
      tabIndex: idx
    });
  };

  showIdx =()=>{
    console.log(this.props.key);
  }

  render() {
    return (
      <div className="tabsBox">
        <div className="tabsBox__tabs">
          {tabsData.map(tab => {
            return (
              <div className="tab" key={tab.id} onClick={this.showIdx}>
                {tab.tabName}
              </div>
            );
          })}
        </div>
        <div className="tabsBox__panels">
          {tabsData.map(tab => {
            return (
              <div
                className="panel"
                key={tab.id}
                dangerouslySetInnerHTML={{ __html: tab.tabContent }}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

export default TabsBox;
