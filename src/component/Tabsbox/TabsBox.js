import React, { Component } from "react";
import "./TabsBox.scss";
import cp1 from "./content_tab1.png";
import cp2 from "./content_tab2.png";
import cp3 from "./content_tab3.png";
import cp4 from "./content_tab4.png";

const tabsData = [
  { id: "1", tabName: "祝福一", tabContent: cp1 },
  { id: "2", tabName: "祝福二", tabContent: cp2 },
  { id: "3", tabName: "祝福三", tabContent: cp3 },
  { id: "4", tabName: "祝福四", tabContent: cp4 }
];

class TabsBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabIndex: 0
    };
  }

  setTabIndex = idx => {
    this.setState({
      tabIndex: idx
    });
  };

  render() {
    return (
      <div className="tabsBox">
        <div className="tabsBox__tabs">
          {tabsData.map(tab => {
            return (
              <div
                className={
                  this.state.tabIndex === tab.id-1 ? "tab active" : "tab"
                }
                key={tab.id}
                onClick={() => {
                  this.setTabIndex(tab.id - 1);
                }}
              >
                {tab.tabName}
              </div>
            );
          })}
        </div>

        <div className="tabsBox__panels">
          <div className="panel">
            <img src={tabsData[this.state.tabIndex].tabContent} alt="" />
          </div>
        </div>
      </div>
    );
  }
}

export default TabsBox;
